IMAGE_NAME = mlops_agent_app

all: build run

build:
	docker build -t $(IMAGE_NAME) .

run:
	docker run -d --name mlops-app -p 80:80 $(IMAGE_NAME)

install:
	pip install -r requirements.txt

lint:
	flake8 app
	black app
