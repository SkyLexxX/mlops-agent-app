from enum import IntEnum

from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np

from app.ppo_model.model import ppo_model

app = FastAPI()


class ActionType(IntEnum):
    NO_ACTION = 0
    LEFT_ENGINE = 1
    MAIN_ENGINE = 2
    RIGHT_ENGINE = 3


class InputData(BaseModel):
    horizontalPadCoordinate: float
    verticalPadCoordinate: float
    horizontalSpeed: float
    verticalSpeed: float
    angle: float
    angularSpeed: float
    leftLegContact: int
    rightLegContact: int


class OutputData(BaseModel):
    action: ActionType


@app.post("/api/inference", response_model=OutputData)
async def inference(input_data: InputData):
    custom_obs_values = np.array(
        [
            input_data.horizontalPadCoordinate,
            input_data.verticalPadCoordinate,
            input_data.horizontalSpeed,
            input_data.verticalSpeed,
            input_data.angle,
            input_data.angularSpeed,
            input_data.leftLegContact,
            input_data.rightLegContact,
        ]
    )
    obs = np.expand_dims(custom_obs_values, axis=0)
    action, _ = ppo_model.model.predict(obs)

    return OutputData(action=ActionType(action[0]))
