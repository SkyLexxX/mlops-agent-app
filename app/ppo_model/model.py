import gymnasium as gym
from stable_baselines3 import PPO


class PPOModel:
    def __init__(self):
        self.model_path = "app/ppo_model/ppo-LunarLander-v2.zip"
        self.env = gym.make("LunarLander-v2")
        self.model = PPO.load(self.model_path, env=self.env)


ppo_model = PPOModel()
