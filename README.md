# MLOps-Agent-App

## Objective
1. Wrap the reinforcement learning ML model of the agent using one of the REST API web frameworks in python (Flask, FastAPI, Django, Tornado, etc...).
2. Create a Dockerfile
3. Create a Makefile with 4 commands: creating a docker image, starting a docker container, installing dependencies/libraries and checking code quality (code-linters)
4. Assemble the docker image that will run the REST API application
5. Download the docker image on DockerHub from your own account

Other requirements:
- The model and its weights should be taken from the previous task (#3, training RL agents)
- A REST API must have an endpoint of /api/inference and conform to the API contract.
- The web server must be running on port 8000
- Docker images on DockerHub should be publicly available

## Prerequisites

Before running the application, make sure you have the following installed:

- Docker: [Get Docker](https://docs.docker.com/get-docker/)
- Python 3.9: [Download Python](https://www.python.org/downloads/release)


## Getting Started

```
git clone https://gitlab.com/SkyLexxX/mlops-agent-app.git
cd mlops-agent-app
```

## How to run

Run the following command to launch locally
```
uvicorn app.main:app --host 0.0.0.0 --port 80
```

Run the Makefile command to launch with the docker
```
make all
```
